class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.text :condition
      t.text :error_code
      t.text :solution
      t.text :explanation

      t.timestamps
    end
  end
end
