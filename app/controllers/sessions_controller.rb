class SessionsController < ApplicationController

  def create
    auth = request.env["omniauth.auth"]
    unless @user = User.find_by_provider_and_uid(auth[:provider], auth[:uid])
      User.create_with_omniauth(auth)
    end
  end

end
