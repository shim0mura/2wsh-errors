class User < ActiveRecord::Base

  def self.create_with_omniauth(auth)
    self.create!({
      :provider  => auth[:provider],
      :uid       => auth[:uid],
      :name      => auth[:info][:nickname],
      :image_url => auth[:info][:image]
    })
  end

end
